﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavmeshAgentManager : MonoBehaviour {
	
	private NavMeshAgent agent;

	private Transform player;

	private void Awake() {
		agent = GetComponent<NavMeshAgent>();
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	// Start is called before the first frame update
	void Start() {
		agent.speed = GameManager.enemySpeed;
	}

	// Update is called once per frame
	void Update() {
		agent.SetDestination(player.position);
	}
}

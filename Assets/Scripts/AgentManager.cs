﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour {

    public float speed;

    private float lookRadius;
	private float distanceRadius;

	[HideInInspector]
	public Vector3 alignment;
	public Vector3 position;
	public Vector3 newVel;

	private Vector3 direction;

	private Transform player;

	private Rigidbody2D rb;

	private GameManager gameManager;

	private void Awake() {
		player = GameObject.FindGameObjectWithTag("Player").transform;
		rb = GetComponent<Rigidbody2D>();
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
	}

	// Start is called before the first frame update
	void Start() {
		speed = GameManager.flockingSpeed;
		lookRadius = 2f;
		distanceRadius = 1.5f;
}

    // Update is called once per frame
    void Update() {
		if (GameManager.gameState == GameManager.GameStates.Playing) {
			AgentMovement();
		}
	}

	private void AgentMovement() {
		int agentsCount = 0;

		alignment = new Vector3(1, 1, 0);
		position = new Vector3(0, 0, 0);
		newVel = new Vector3(0, 0, 0);
		direction = new Vector3(0, 0, 0);

		for (int i = 0; i < gameManager.agents.Count; i++) {
			if (gameManager.agents[i] != this.gameObject) {
				float distance = Vector3.Distance(transform.position, gameManager.agents[i].transform.position);

				if (distance <= lookRadius) {
					alignment += gameManager.agents[i].GetComponent<AgentManager>().alignment;

                    position += gameManager.agents[i].GetComponent<AgentManager>().transform.position;

					if (distance <= distanceRadius) {
                        newVel = gameManager.agents[i].GetComponent<AgentManager>().transform.position - transform.position;
                        newVel *= -1;
					}

					agentsCount++;
				}
			}
		}

		direction = player.position - transform.position;
		direction *= 50;

		if (transform.position.x < -5) {
			transform.position += new Vector3(0.05f, 0, 0);
		} else if (transform.position.x > 6) {
			transform.position -= new Vector3(0.05f, 0, 0);
		}

		if (transform.position.y < -4) {
			transform.position += new Vector3(0, 0.05f, 0);
		} else if (transform.position.y > 4) {
			transform.position -= new Vector3(0, 0.05f, 0);
		}

		if (agentsCount == 0) {
			agentsCount = 1;
		}

        alignment += direction;
		alignment /= agentsCount;
        alignment.Normalize();
        alignment *= 0.6f;
        
		position /= agentsCount;
        position.Normalize();
        position *= 0.2f;

        newVel.Normalize();
        newVel *= 0.2f;

        transform.position += (alignment + position + newVel) * speed * Time.deltaTime;
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
	}
}

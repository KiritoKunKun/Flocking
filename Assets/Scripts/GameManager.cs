﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static float enemySpeed;
	private static float enemyBaseSpeed;

	public static float flockingSpeed;
	private static float flockingBaseSpeed;

	public string level;

	private bool isInitial;

	public GameObject agent;

	public List<GameObject> agents;

	public Text winRateText;

	public enum GameStates { Playing, Initial }
	public static GameStates gameState;

	private void Awake() {
		enemyBaseSpeed = 70f;
		flockingBaseSpeed = 20f;

		if (PlayerPrefs.HasKey("WinRate")) {
			enemySpeed = enemyBaseSpeed * PlayerPrefs.GetFloat("WinRate");
			flockingSpeed = flockingBaseSpeed * PlayerPrefs.GetFloat("WinRate");

			winRateText.text = "Win Rate: " + PlayerPrefs.GetFloat("WinRate") * 100 + "%";
		} else {
			enemySpeed = 50f;
			flockingSpeed = 5f;

			winRateText.text = "Win Rate: " + 1 * 100 + "%";
		}

		isInitial = true;
	}

	// Start is called before the first frame update
	void Start() {
		gameState = GameStates.Initial;

		if (level == "Flocking") {
			for (int i = 0; i < 10; i++) {
				float randomX = Random.Range(-3f, 6f);
				float randomY = Random.Range(-4f, 4f);

				GameObject ag = Instantiate(agent, new Vector3(randomX, randomY, 0), Quaternion.identity);
				agents.Add(ag);
			}
		} else if (level == "PathMoving") {

		} else if (level == "PathFinding") {

		}
	}

	// Update is called once per frame
	void Update() {
		if (isInitial) {
			if (Input.GetMouseButtonDown(0)) {
				gameState = GameStates.Playing;
			}
		}
	}
}

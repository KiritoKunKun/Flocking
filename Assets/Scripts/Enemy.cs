﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public Vector2 startPos;
	public Vector2 finalPos;

	public bool isRight;
	public bool startUp;

	public float speed;

	// Start is called before the first frame update
	void Start() {
		speed = GameManager.enemySpeed;

		if (startUp) {
			transform.position = finalPos;
		} else {
			transform.position = startPos;
		}
	}

	// Update is called once per frame
	void Update() {
		if (GameManager.gameState == GameManager.GameStates.Playing) {
			EnemyMovement();
		}
	}

	private void EnemyMovement() {
		Vector2 startDistance = startPos - (Vector2)transform.position;
		Vector2 finalDistance = finalPos - (Vector2)transform.position;
		Vector2 dir;

		if (transform.position.y >= finalPos.y) {
			isRight = false;
		} else if (transform.position.y <= startPos.y) {
			isRight = true;
		}

		if (isRight) {
			dir = finalDistance.normalized * speed * Time.deltaTime;
		} else {
			dir = startDistance.normalized * speed * Time.deltaTime;
		}

		transform.Translate(dir * 0.1f);
	}
}

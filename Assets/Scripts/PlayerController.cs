﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	public float speed;

	private GameManager gameManager;

	private void Awake() {
		gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
	}

	// Start is called before the first frame update
	void Start() {
		if (PlayerPrefs.HasKey("PlayerSpeed")) {
			speed = PlayerPrefs.GetFloat("PlayerSpeed");
		} else {
			speed = 8f;
		}
	}

	// Update is called once per frame
	void Update() {
		if (GameManager.gameState == GameManager.GameStates.Playing) {
			PlayerMovement();
		}
	}

	public void PlayerMovement() {
		Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		transform.position = Vector2.Lerp(transform.position, mousePos, speed * Time.deltaTime);
		
		if (gameManager.level != "Flocking") {
			if (transform.position.y < -3) {
				transform.position = new Vector3(transform.position.x, -3, 0);
			} else if (transform.position.y > 3) {
				transform.position = new Vector3(transform.position.x, 3, 0);
			}
		} else {
			if (transform.position.y < -4) {
				transform.position = new Vector3(transform.position.x, -4, 0);
			} else if (transform.position.y > 4) {
				transform.position = new Vector3(transform.position.x, 4, 0);
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D coll) {
		if (coll.gameObject.tag == "Enemy") {
			float totalDepartures = 0;
			float winDepartures = 0;
			float winRate = 0f;

			if (PlayerPrefs.HasKey("TotalDepartures")) {
				totalDepartures = PlayerPrefs.GetFloat("TotalDepartures");
				totalDepartures++;
			} else {
				totalDepartures = 1;
			}

			if (PlayerPrefs.HasKey("WinDepartures")) {
				winDepartures = PlayerPrefs.GetFloat("WinDepartures");
			} else {
				winDepartures = 1;
			}

			if (PlayerPrefs.HasKey("WinRate")) {
				winRate = PlayerPrefs.GetFloat("WinRate");
			} else {
				winRate = 0f;
			}

			winRate = winDepartures / totalDepartures;

			PlayerPrefs.SetFloat("TotalDepartures", totalDepartures);
			PlayerPrefs.SetFloat("WinDepartures", winDepartures);
			PlayerPrefs.SetFloat("WinRate", winRate);

			SceneManager.LoadScene("GameOver");
		} else if (coll.gameObject.tag == "Win") {
			//Change Fuzzy values
			float totalDepartures = 0f;
			float winDepartures = 0f;
			float winRate = 0f;

			if (PlayerPrefs.HasKey("TotalDepartures")) {
				totalDepartures = PlayerPrefs.GetFloat("TotalDepartures");
				totalDepartures++;
			} else {
				totalDepartures = 1;
			}

			if (PlayerPrefs.HasKey("WinDepartures")) {
				winDepartures = PlayerPrefs.GetFloat("WinDepartures");
				winDepartures++;
			} else {
				winDepartures = 1;
			}

			if (PlayerPrefs.HasKey("WinRate")) {
				winRate = PlayerPrefs.GetFloat("WinRate");
			} else {
				winRate = 1f;
			}

			winRate = winDepartures / totalDepartures;

			PlayerPrefs.SetFloat("TotalDepartures", totalDepartures);
			PlayerPrefs.SetFloat("WinDepartures", winDepartures);
			PlayerPrefs.SetFloat("WinRate", winRate);

			if (gameManager.level == "Flocking") {
				SceneManager.LoadScene("PathMoving");
			} else if (gameManager.level == "PathMoving") {
				SceneManager.LoadScene("Flocking");
			} else if (gameManager.level == "PathFinding") {
				SceneManager.LoadScene("Flocking");
			}
		}
	}
}
